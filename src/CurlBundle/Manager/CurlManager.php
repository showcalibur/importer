<?php

namespace CurlBundle\Manager;

class CurlManager
{
    private const BUFFER_SIZE = 250000; 

    /**
     * function getFileSize.
     *
     * NOTE: Original code taken from 
     * https://stackoverflow.com/questions/2602612/remote-file-size-without-downloading-file
     *
     * Returns the size of a file without downloading it, or -1 if the file
     * size could not be determined.
     *
     * @param $url - The location of the remote file to download. Cannot
     *             be null or empty.
     *
     * @return The size of the file referenced by $url, or -1 if the size
     * could not be determined.
     */
    function getFileSize(string $url)
    {
        // Assume failure.
        $result = -1;

        $curl = curl_init($url);

        // Disable any certificates
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // Issue a HEAD request and follow any redirects.
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8');

        $data = curl_exec($curl);

        curl_close($curl);

        if($data ) {
            $content_length = "unknown";
            $status = "unknown";

            if(preg_match("/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches) ) {
                $status = (int)$matches[1];
            }

            if(preg_match("/Content-Length: (\d+)/", $data, $matches) ) {
                $content_length = (int)$matches[1];
            }

            // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            if($status == 200 || ($status > 300 && $status <= 308) ) {
                $result = $content_length;
            }
        }

        return $result;
    }

    /**
     * function getFileSize.
     *
     * NOTE: Original code taken from 
     * https://stackoverflow.com/questions/2032924/how-to-partially-download-a-remote-file-with-curl
     * 
     * Download file partially from Internet.
     *
     * @return array
     */
    public function getFilePartial(string $url, int $start = 0, int $stop = 1000)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RANGE, "$start-$stop");
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function getFileBuffered(string $url, $callback)
    {
        $fileSize = $this->getFileSize($url);
        $buffer = null;
        $next_i = 0;
        $n = 0;
        for ($i=0; $i<$fileSize; $i=$next_i) {
            $buffer = $this->getFilePartial($url, $i, $i + self::BUFFER_SIZE);
            
            if ("{\"order_id\":" <> substr($buffer, 0, 12)) { die("Data corrupted!");
            }

            $lines = explode("\n", $buffer);            
            $lastline = trim(end($lines));
            if (!empty($lastline)) {
                $next_i = $i + strpos($buffer, $lastline);
                array_pop($lines);
                $data = implode("\n", $lines);
            } else {
                $next_i = $fileSize;
                $data = $buffer;
            }
            $callback($data);
        }
    }
}