<?php

namespace ImporterBundle\Manager;

use CurlBundle\Manager\CurlManager;

class ImporterManager
{
    /**
     * @var CurlManager
     */
    protected CurlManager $curlManager;

    /**
     * @var string
     */
    protected string $in;

    /**
     * @var string
     */
    protected string $out;

    /**
     * ImporterManager constructor.
     * 
     * @param string $input_file
     * @param string $output_file
     */
    public function __construct(CurlManager $curlManager, string $in, string $out)
    {
        $this->curlManager = $curlManager;
        $this->in = $in;
        $this->out = $out;
    }

    /**
     * Import data.
     */
    public function importData($callback)
    {
        $this->curlManager->getFileBuffered($this->in, $callback);
    }

    /**
     * Get total order value.
     *
     * @return float
     */
    public function getTotalOrderValue(array $items)
    {
        $total = 0.0;
        foreach ($items as $item) {
            $total += $item->unit_price;
        }
        return $total;
    }

    /**
     * Get average unit price.
     *
     * @return float
     */
    public function getAverageUnitPrice(array $items)
    {
        $sum = 0.0;
        $count = 0;
        foreach ($items as $item) {
            $sum += $item->unit_price;
            $count++;
        }
        return ($sum / $count);
    }

    /**
     * Get distinct unit count.
     *
     * @return int
     */
    public function getDistinctUnitCount(array $items)
    {
        $products = array();
        $count = 0;
        foreach ($items as $item) {
            if (!in_array($item->product->product_id, $products)) {
                $products[] = $item->product->product_id;
                $count++;
            }
        }
        return $count;
    }

    /**
     * Get total units count.
     *
     * @return int
     */
    public function getTotalUnitsCount(array $items)
    {
        return count($items);
    }

    /**
     * Get customer state.
     *
     * @return string
     */
    public function getCustomerState(object $customer)
    {
        $state = $customer->shipping_address->state;
        $st = explode(' ', $state);
        foreach ($st as $key => $value) {
            $st[$key] = ucfirst(strtolower($value));
        }
        $state = implode(' ', $st);
        return $state;
    }
}
