<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Oshomo\CsvUtils\Validator\Validator;
use Oshomo\CsvUtils\Converter\JsonConverter;
use Oshomo\CsvUtils\Converter\XmlConverter;

class AppImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:import')
            ->setDescription('Import data from JSONL and process them to CSV')
            ->addArgument('email', InputArgument::OPTIONAL, 'E-mail to which the output should be sent')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // read email argument (if any)
        $email = $input->getArgument('email');

        if ($input->getOption('option')) {
            // ...
        }
        
        $outFile = $this->getContainer()->getParameter('csv_file');
        echo "Output file: $outFile\n";

        // put fields into first line
        $fields = array();
        $fields[] = 'order_id';
        $fields[] = 'order_datetime';
        $fields[] = 'total_order_value';
        $fields[] = 'average_unit_price';
        $fields[] = 'distinct_unit_count';
        $fields[] = 'total_units_count';
        $fields[] = 'customer_state';
        $row = '"' . implode('","', $fields) . "\"\n";
        file_put_contents($outFile, $row);

        // import data
        $importerManager = $this->getContainer()->get('importer.manager');
        echo "\nStart importing data...\n";
        $importerManager->importData(
            function (string $rawData) use ($importerManager, $outFile) {
                echo "\nPartial data downloaded:\n";

                // read per line
                $arrData = explode("\n", $rawData);
                foreach ($arrData as $jsonData) {
                    // skip if reach end of data
                    if (empty($jsonData)) { continue;
                    }

                    // decode JSON
                    $data = json_decode(trim($jsonData));
                    echo "ORDER ID: $data->order_id\n";

                    // process date
                    $datetime = new \DateTime($data->order_date);
                    $datetime_iso8601 = $datetime->format('c');
                    echo "ORDER DATETIME: $data->order_date\n";
                    echo "ORDER DATETIME (ISO 8601): $datetime_iso8601\n";

                    // calculate total order value
                    $totalOrderValue = $importerManager->getTotalOrderValue($data->items);
                    echo "  -- totalOrderValue = $totalOrderValue\n";

                    // skip if zero
                    if ($totalOrderValue == 0) { continue;
                    }

                    // calculate other columns
                    $averageUnitPrice = $importerManager->getAverageUnitPrice($data->items);
                    echo "  -- averageUnitPrice = $averageUnitPrice\n";
                    $distinctUnitCount = $importerManager->getDistinctUnitCount($data->items);
                    echo "  -- distinctUnitCount = $distinctUnitCount\n";
                    $totalUnitsCount = $importerManager->getTotalUnitsCount($data->items);
                    echo "  -- totalUnitsCount = $totalUnitsCount\n";
                    $customerState = $importerManager->getCustomerState($data->customer);
                    echo "  -- customerState = $customerState\n";

                    // construct row data
                    $cols = array();
                    $cols[] = $data->order_id;
                    $cols[] = $datetime_iso8601;
                    $cols[] = $totalOrderValue;
                    $cols[] = $averageUnitPrice;
                    $cols[] = $distinctUnitCount;
                    $cols[] = $totalUnitsCount;
                    $cols[] = $customerState;
                    $row = '"' . implode('","', $cols) . "\"\n";

                    // append to file
                    file_put_contents($outFile, $row, FILE_APPEND | LOCK_EX);
                }
            }
        );

        $validator = new Validator($outFile, ',', []);

        // validate CSV programatically
        if(!$validator->fails()) {
            // write output also in JSON and XML
            $validator->write(new JsonConverter());
            $validator->write(new XmlConverter("order"));
        }

        // if not valid
        $validator->fails() && die(print_r($validator->errors(), true));

        $output->writeln('Complete.');
    }
}
