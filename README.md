Importer Application
========================

Author: Reza Klifartha

E-mail: novareza@hotmail.com, showcalibur@gmail.com

## Framework and Structure

Application is built on top of Symfony Framework 3.4 by issuing these command:

``composer create-project symfony/framework-standard-edition app "3.4.*"``

All unused bundles and configurations are disabled and/or removed.

Three layer of applications:

 - AppBundle : User Interface via CLI / command-line
   Handling user commandline execution

 - CurlBundle : Communication Interface with Internet (HTTP)
   Handling file download and its buffering method

 - ImporterBundle : Data Processing Engine
   Handling data parsing and processing/calculating

They are all implemented using Dependency Injection with autowire feature enabled,
So objects will be automatically created and injected when it's needed.

Download function is designed to download file with ability to get only partial bytes
of total bytes available. This approach will minimise the risk of connection timeout
problem for huge size of file.

Every data calculation functions are covered by unit tests.

## Setup and Deployment

Clone the project by issuing these command:

``git clone https://showcalibur@bitbucket.org/showcalibur/importer.git``

Go to project folder then:

``composer update``

Default value for application will be added to file: ``app/config/parameters.yml``

```
jsonl_file ('https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl'):
csv_file (out.csv):
```

Run simple PHP unit for the first time (required to setup its composer dependencies):
``php ./vendor/bin/simple-phpunit``

When tests run without any issue, then application is ready to use.


## Test Driven Development and Code Coverage

This application is built with TDD[1] in mind, so I specified all of small functionality 
in unit tests before put them all into main implementation.

Run this command once to trigger simple-phpunit setup:

``php ./vendor/bin/simple-phpunit``

Later, use this command to do test during TDD:

``composer test``

or

``composer test -- <folder path or file>``

  Example: ``composer test -- tests\CurlBundle\Manager\CurlManagerTest.php``

![image](https://i.imgur.com/o27FGTZ.png)

NOTE: This requires XDebug zend extension to be installed for PHP.

``composer run coverage``

It will generate code coverage in HTML format, showing us percentage of code covered by tests

![image](https://i.imgur.com/CfhwNMN.png)


## Code Beautifier / Linter [2]

Run this command to analyze PHP code:

``composer run beautify``

![image](https://i.imgur.com/CS317CJ.png)

## Command Line Implementation

Type this command and hit ENTER:
``php bin/console app:import``

When process completed, it will say "Completed."

This command will output CSV (out.csv) as primary output.
In addition, it also generates JSON and XML files (out.json, out.xml)

![image](https://i.imgur.com/ql8btOl.png)

![image](https://i.imgur.com/Y0uME8Q.png)

![image](https://i.imgur.com/D0WWwl0.png)

Enjoy!

[1]: https://www.guru99.com/test-driven-development.html
[2]: https://phpqa.io/projects/phpcbf.html

