<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImporterManagerTest extends KernelTestCase
{
    private $importerManager;

    protected function setUp()
    {
        self::bootKernel();

        $this->importerManager = static::$kernel
            ->getContainer()
            ->get('importer.manager');
    }

    public function testImportData()
    {
        $im = $this->importerManager;
        $im->importData(
            function (string $rawData) use ($im) {
                echo "\nPartial data downloaded:\n";
                ob_flush();

                $this->assertEquals("{\"order_id\":", substr($rawData, 0, 12));

                $arrData = explode("\n", $rawData);
                foreach ($arrData as $jsonData) {
                    if (empty($jsonData)) { continue;
                    }

                    $data = json_decode(trim($jsonData));
                    echo "ORDER ID: $data->order_id\n";

                    $datetime = new DateTime($data->order_date);
                    $datetime_iso8601 = $datetime->format('c');
                    echo "ORDER DATETIME: $data->order_date\n";
                    echo "ORDER DATETIME (ISO 8601): $datetime_iso8601\n";

                    $totalOrderValue = $im->getTotalOrderValue($data->items);
                    echo "  -- totalOrderValue = $totalOrderValue\n";
                    $averageUnitPrice = $im->getAverageUnitPrice($data->items);
                    echo "  -- averageUnitPrice = $averageUnitPrice\n";
                    $distinctUnitCount = $im->getDistinctUnitCount($data->items);
                    echo "  -- distinctUnitCount = $distinctUnitCount\n";
                    $totalUnitsCount = $im->getTotalUnitsCount($data->items);
                    echo "  -- totalUnitsCount = $totalUnitsCount\n";
                    $customerState = $im->getCustomerState($data->customer);
                    echo "  -- customerState = $customerState\n";

                    // if ($distinctUnitCount <> $totalUnitsCount) {
                    //     print_r($data);
                    //     exit;
                    // }

                    ob_flush();

                    $this->assertEquals('object', gettype($data));
                }
            }
        );
    }
}