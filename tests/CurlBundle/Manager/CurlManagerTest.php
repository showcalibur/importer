<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CurlManagerTest extends KernelTestCase
{
    private $curlManager;

    protected function setUp()
    {
        self::bootKernel();

        $this->curlManager = static::$kernel
            ->getContainer()
            ->get('curl.manager');
    }

    public function testGetFilePartial()
    {
        $theFile = 'https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl';
        $fileSize = $this->curlManager->getFileSize($theFile);
        $buffer = null;
        $next_i = 0;
        $n = 0;
        echo "File Size = $fileSize\n";
        for ($i=0; $i<$fileSize; $i=$next_i) {
            echo "Downloading $i...\n";
            $buffer = $this->curlManager->getFilePartial($theFile, $i, $i+100000);
            echo "  --". substr($buffer, 0, 17). "\n";

            $this->assertEquals("{\"order_id\":", substr($buffer, 0, 12));

            $lines = explode("\n", $buffer);            
            $lastline = trim(end($lines));
            if (!empty($lastline)) {
                echo "  -- -- ". substr($buffer, strpos($buffer, $lastline), 17). "\n";
                $next_i = $i + strpos($buffer, $lastline);
                array_pop($lines);
                $data = implode("\n", $lines);
            } else {
                $next_i = $fileSize;
                $data = $buffer;
            }
            
            $this->assertEquals("{\"order_id\":", substr($data, 0, 12));

            ob_flush();
        }
    }

    public function testGetFileBuffered()
    {
        $theFile = 'https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl';
        $this->curlManager->getFileBuffered(
            $theFile, function (string $data) {
                echo "Callback triggered:\n";
                echo "  -- ". substr($data, 0, 17). "\n";
                $this->assertEquals("{\"order_id\":", substr($data, 0, 12));
                ob_flush();
            }
        );
    }
    
}