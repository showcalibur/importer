<?php

use AppBundle\Command\AppImportCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class AppImportCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(new AppImportCommand());

        $command = $application->find('app:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
            'command' => $command->getName()
            )
        );

        $output = $commandTester->getDisplay();
        $this->assertContains('Complete', $output);
    }
}